package com.example.alina.announcementapp.util;

public interface OnSuccessListener<E> {
    void onSuccess(E e);
}
