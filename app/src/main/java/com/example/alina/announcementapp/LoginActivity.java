package com.example.alina.announcementapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.example.alina.announcementapp.content.User;
import com.example.alina.announcementapp.service.AnnouncementManager;
import com.example.alina.announcementapp.util.Cancellable;
import com.example.alina.announcementapp.util.DialogUtils;
import com.example.alina.announcementapp.util.OnErrorListener;
import com.example.alina.announcementapp.util.OnSuccessListener;

public class LoginActivity extends AppCompatActivity {

  private Cancellable mCancellable;
  private AnnouncementManager mAnnouncementManager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    mAnnouncementManager = ((AnnouncementApp) getApplication()).getAnnouncementManager();
    User user = mAnnouncementManager.getCurrentUser();
    if (user != null) {
      startAnnouncementListActivity();
      finish();
    }
    setupToolbar();
  }

  @Override
  protected void onStop() {
    super.onStop();
    if (mCancellable != null) {
      mCancellable.cancel();
    }
  }

  private void setupToolbar() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        login();
        Snackbar.make(view, "Authenticating, please wait", Snackbar.LENGTH_INDEFINITE)
            .setAction("Action", null).show();
      }
    });
  }

  private void login() {
    EditText usernameEditText = (EditText) findViewById(R.id.username);
    EditText passwordEditText = (EditText) findViewById(R.id.password);
    mCancellable = mAnnouncementManager
        .loginAsync(
            usernameEditText.getText().toString(), passwordEditText.getText().toString(),
            new OnSuccessListener<String>() {
              @Override
              public void onSuccess(String s) {
                runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                    startAnnouncementListActivity();
                  }
                });
              }
            }, new OnErrorListener() {
              @Override
              public void onError(final Exception e) {
                runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                    DialogUtils.showError(LoginActivity.this, e);
                  }
                });
              }
            });
  }

  private void startAnnouncementListActivity() {
    startActivity(new Intent(this, AnnouncementListActivity.class));
  }
}
