package com.example.alina.announcementapp.net.mapping;

import android.util.JsonWriter;

import com.example.alina.announcementapp.content.Announcement;

import java.io.IOException;

import static com.example.alina.announcementapp.net.mapping.Api.Announcement.DATE;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.TEXT;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.TITLE;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.UPDATED;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.USER_ID;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.VERSION;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement._ID;


public class AnnouncementWriter implements ResourceWriter<Announcement, JsonWriter>{
  @Override
  public void write(Announcement announcement, JsonWriter writer) throws IOException {
    writer.beginObject();
    {
      if (announcement.getId() != null) {
        writer.name(_ID).value(announcement.getId());
      }
      writer.name(TEXT).value(announcement.getText());
      writer.name(TITLE).value(announcement.getTitle());
      writer.name(DATE).value(announcement.getDate());
      if (announcement.getUpdated() > 0) {
        writer.name(UPDATED).value(announcement.getUpdated());
      }
      if (announcement.getUserId() != null) {
        writer.name(USER_ID).value(announcement.getUserId());
      }
      if (announcement.getVersion() > 0) {
        writer.name(VERSION).value(announcement.getVersion());
      }
    }
    writer.endObject();
  }
}
