package com.example.alina.announcementapp.service;

import android.content.Context;
import android.util.Log;

import com.example.alina.announcementapp.content.Announcement;
import com.example.alina.announcementapp.net.LastModifiedList;
import com.example.alina.announcementapp.util.CancellableCallable;
import com.example.alina.announcementapp.util.OkAsyncTaskLoader;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class AnnouncementLoader extends OkAsyncTaskLoader<List<Announcement>> implements Observer {
  private static final String TAG = AnnouncementLoader.class.getSimpleName();
  private final AnnouncementManager mAnnouncementManager;
  private List<Announcement> mCachedAnnouncements;
  private CancellableCallable<LastModifiedList<Announcement>> mCancellableCall;

  public AnnouncementLoader(Context context, AnnouncementManager announcementManager) {
    super(context);
    mAnnouncementManager = announcementManager;
  }

  @Override
  public List<Announcement> tryLoadInBackground() throws Exception {
    // This method is called on a background thread and should generate a
    // new set of data to be delivered back to the client
    Log.d(TAG, "tryLoadInBackground");
    mCancellableCall = mAnnouncementManager.getAnnouncementsCall();
    mCachedAnnouncements = mAnnouncementManager.executeAnnouncementsCall(mCancellableCall);
    return mCachedAnnouncements;
  }

  @Override
  public void deliverResult(List<Announcement> data) {
    Log.d(TAG, "deliverResult");
    if (isReset()) {
      Log.d(TAG, "deliverResult isReset");
      // The Loader has been reset; ignore the result and invalidate the data.
      return;
    }
    mCachedAnnouncements = data;
    if (isStarted()) {
      Log.d(TAG, "deliverResult isStarted");
      // If the Loader is in a started state, deliver the results to the
      // client. The superclass method does this for us.
      super.deliverResult(data);
    }
  }

  @Override
  protected void onStartLoading() {
    Log.d(TAG, "onStartLoading");
    if (mCachedAnnouncements != null) {
      Log.d(TAG, "onStartLoading cached not null");
      // Deliver any previously loaded data immediately.
      deliverResult(mCachedAnnouncements);
    }
    // Begin monitoring the underlying data source.
    mAnnouncementManager.addObserver(this);
    if (takeContentChanged() || mCachedAnnouncements == null) {
      // When the observer detects a change, it should call onContentChanged()
      // on the Loader, which will cause the next call to takeContentChanged()
      // to return true. If this is ever the case (or if the current data is
      // null), we force a new load.
      Log.d(TAG, "onStartLoading cached null force reload");
      forceLoad();
    }
  }

  @Override
  protected void onStopLoading() {
    // The Loader is in a stopped state, so we should attempt to cancel the
    // current load (if there is one).
    Log.d(TAG, "onStopLoading");
    cancelLoad();
    // Announcement that we leave the observer as is. Loaders in a stopped state
    // should still monitor the data source for changes so that the Loader
    // will know to force a new load if it is ever started again.
  }

  @Override
  protected void onReset() {
    // Ensure the loader has been stopped.
    Log.d(TAG, "onReset");
    onStopLoading();
    // At this point we can release the resources associated with 'mData'.
    if (mCachedAnnouncements != null) {
      mCachedAnnouncements = null;
    }
    // The Loader is being reset, so we should stop monitoring for changes.
    mAnnouncementManager.deleteObserver(this);
  }

  @Override
  public void onCanceled(List<Announcement> data) {
    // Attempt to cancel the current asynchronous load.
    Log.d(TAG, "onCanceled");
    super.onCanceled(data);
  }

  @Override
  public void update(Observable o, Object arg) {
    mCachedAnnouncements = mAnnouncementManager.getCachedAnnouncements();
  }
}
