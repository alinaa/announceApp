package com.example.alina.announcementapp.net;

import android.content.Context;
import android.util.Log;


import com.example.alina.announcementapp.R;
import com.example.alina.announcementapp.content.Announcement;
import com.example.alina.announcementapp.net.mapping.AnnouncementJsonObjectReader;
import com.example.alina.announcementapp.net.mapping.IdJsonObjectReader;

import org.json.JSONObject;


import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.example.alina.announcementapp.net.mapping.Api.Announcement.ANNOUNCEMENT_CREATED;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.ANNOUNCEMENT_DELETED;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.ANNOUNCEMENT_UPDATED;


public class AnnouncementSocketClient {
  private static final String TAG = AnnouncementSocketClient.class.getSimpleName();
  private final Context mContext;
  private Socket mSocket;
  private ResourceChangeListener<Announcement> mResourceListener;

  public AnnouncementSocketClient(Context context) {
    mContext = context;
    Log.d(TAG, "created");
  }

  public void subscribe(final ResourceChangeListener<Announcement> resourceListener) {
    Log.d(TAG, "subscribe");
    mResourceListener = resourceListener;
    try {
      mSocket = IO.socket(mContext.getString(R.string.api_url));
      mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
        @Override
        public void call(Object... args) {
          Log.d(TAG, "socket connected");
        }
      });
      mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
        @Override
        public void call(Object... args) {
          Log.d(TAG, "socket disconnected");
        }
      });
      mSocket.on(ANNOUNCEMENT_CREATED, new Emitter.Listener() {
        @Override
        public void call(Object... args) {
          try {
            Announcement announcement = new AnnouncementJsonObjectReader().read((JSONObject) args[0]);
            Log.d(TAG, String.format("announcement created %s", announcement.toString()));
            mResourceListener.onCreated(announcement);
          } catch (Exception e) {
            Log.w(TAG, "note created", e);
            mResourceListener.onError(new ResourceException(e));
          }
        }
      });
      mSocket.on(ANNOUNCEMENT_UPDATED, new Emitter.Listener() {
        @Override
        public void call(Object... args) {
          try {
            Announcement announcement = new AnnouncementJsonObjectReader().read((JSONObject) args[0]);
            Log.d(TAG, String.format("announcement updated %s", announcement.toString()));
            mResourceListener.onUpdated(announcement);
          } catch (Exception e) {
            Log.w(TAG, "announcement updated", e);
            mResourceListener.onError(new ResourceException(e));
          }
        }
      });
      mSocket.on(ANNOUNCEMENT_DELETED, new Emitter.Listener() {
        @Override
        public void call(Object... args) {
          try {
            String id = new IdJsonObjectReader().read((JSONObject) args[0]);
            Log.d(TAG, String.format("announcement deleted %s", id));
            mResourceListener.onDeleted(id);
          } catch (Exception e) {
            Log.w(TAG, "announcement deleted", e);
            mResourceListener.onError(new ResourceException(e));
          }
        }
      });
      mSocket.connect();
    } catch (Exception e) {
      Log.w(TAG, "socket error", e);
      mResourceListener.onError(new ResourceException(e));
    }
  }

  public void unsubscribe() {
    Log.d(TAG, "unsubscribe");
    if (mSocket != null) {
      mSocket.disconnect();
    }
    mResourceListener = null;
  }

}
