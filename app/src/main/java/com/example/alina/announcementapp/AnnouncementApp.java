package com.example.alina.announcementapp;

import android.app.Application;
import android.util.Log;

import com.example.alina.announcementapp.net.AnnouncementRestClient;
import com.example.alina.announcementapp.net.AnnouncementSocketClient;
import com.example.alina.announcementapp.service.AnnouncementManager;


public class AnnouncementApp extends Application {
  public static final String TAG = AnnouncementApp.class.getSimpleName();
  private AnnouncementManager mAnnouncementManager;
  private AnnouncementRestClient mAnnouncementRestClient;
  private AnnouncementSocketClient mAnnouncementSocketClient;

  @Override
  public void onCreate() {
    Log.d(TAG, "onCreate");
    super.onCreate();
    mAnnouncementManager = new AnnouncementManager(this);
    mAnnouncementRestClient = new AnnouncementRestClient(this);
    mAnnouncementSocketClient = new AnnouncementSocketClient(this);
    mAnnouncementManager.setAnnouncementRestClient(mAnnouncementRestClient);
    mAnnouncementManager.setAnnouncementSocketClient(mAnnouncementSocketClient);
  }

  public AnnouncementManager getAnnouncementManager() {
    return mAnnouncementManager;
  }

  @Override
  public void onTerminate() {
    Log.d(TAG, "onTerminate");
    super.onTerminate();
  }
}
