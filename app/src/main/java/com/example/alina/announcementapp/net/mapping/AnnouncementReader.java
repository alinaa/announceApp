package com.example.alina.announcementapp.net.mapping;

import android.util.JsonReader;
import android.util.Log;


import com.example.alina.announcementapp.content.Announcement;

import java.io.IOException;

import static android.provider.ContactsContract.ProviderStatus.STATUS;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.CATEGORY;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.DATE;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.TEXT;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.TITLE;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.UPDATED;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.USER_ID;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.VERSION;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement._ID;


public class AnnouncementReader implements ResourceReader<Announcement, JsonReader> {
  private static final String TAG = AnnouncementReader.class.getSimpleName();

  @Override
  public Announcement read(JsonReader reader) throws IOException {
    Announcement announcement = new Announcement();
    reader.beginObject();
    while (reader.hasNext()) {
      String name = reader.nextName();
      if (name.equals(_ID)) {
        announcement.setId(reader.nextString());
      } else if (name.equals(TEXT)) {
        announcement.setText(reader.nextString());
      }else if (name.equals(TITLE)) {
        announcement.setTitle(reader.nextString());
      } else if (name.equals(CATEGORY)) {
        announcement.setCategory(reader.nextString());}
      else if (name.equals(DATE)) {
        announcement.setDate(reader.nextString());
      } else if (name.equals(UPDATED)) {
        announcement.setUpdated(reader.nextLong());
      } else if (name.equals(USER_ID)) {
        announcement.setUserId(reader.nextString());
      } else if (name.equals(VERSION)) {
        announcement.setVersion(reader.nextInt());
      } else {
        reader.skipValue();
        Log.w(TAG, String.format("Announcement property '%s' ignored", name));
      }
    }
    reader.endObject();
    return announcement;
  }
}
