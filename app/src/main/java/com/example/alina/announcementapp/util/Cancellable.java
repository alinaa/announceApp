package com.example.alina.announcementapp.util;

public interface Cancellable {
  void cancel();
}
