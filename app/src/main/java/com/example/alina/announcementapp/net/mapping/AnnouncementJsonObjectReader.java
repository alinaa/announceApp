package com.example.alina.announcementapp.net.mapping;


import com.example.alina.announcementapp.content.Announcement;

import org.json.JSONObject;

import static com.example.alina.announcementapp.net.mapping.Api.Announcement.CATEGORY;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.DATE;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.TEXT;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.TITLE;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.UPDATED;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.USER_ID;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement.VERSION;
import static com.example.alina.announcementapp.net.mapping.Api.Announcement._ID;


public class AnnouncementJsonObjectReader implements ResourceReader<Announcement, JSONObject> {
  private static final String TAG = AnnouncementJsonObjectReader.class.getSimpleName();

  @Override
  public Announcement read(JSONObject obj) throws Exception {
    Announcement announcement = new Announcement();
    announcement.setId(obj.getString(_ID));
    announcement.setText(obj.getString(TEXT));
    announcement.setUpdated(obj.getLong(UPDATED));
    announcement.setTitle(TITLE);
    announcement.setCategory(CATEGORY);
    announcement.setDate(DATE);
    announcement.setUserId(obj.getString(USER_ID));
    announcement.setVersion(obj.getInt(VERSION));
    return announcement;
  }
}
