package com.example.alina.announcementapp.util;

public interface OnErrorListener {
    void onError(Exception e);
}
