package com.example.alina.announcementapp.service;

import android.content.Context;
import android.util.Log;


import com.example.alina.announcementapp.content.Announcement;
import com.example.alina.announcementapp.content.User;
import com.example.alina.announcementapp.content.database.AnnouncementDatabase;
import com.example.alina.announcementapp.net.AnnouncementRestClient;
import com.example.alina.announcementapp.net.AnnouncementSocketClient;
import com.example.alina.announcementapp.net.LastModifiedList;
import com.example.alina.announcementapp.net.ResourceChangeListener;
import com.example.alina.announcementapp.net.ResourceException;
import com.example.alina.announcementapp.util.Cancellable;
import com.example.alina.announcementapp.util.CancellableCallable;
import com.example.alina.announcementapp.util.OnErrorListener;
import com.example.alina.announcementapp.util.OnSuccessListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class AnnouncementManager extends Observable {
  private static final String TAG = AnnouncementManager.class.getSimpleName();
  private final AnnouncementDatabase mKD;

  private ConcurrentMap<String, Announcement> mAnnouncements = new ConcurrentHashMap<String, Announcement>();
  private String mAnnouncementsLastUpdate;

  private final Context mContext;
  private AnnouncementRestClient mAnnouncementRestClient;
  private AnnouncementSocketClient mAnnouncementSocketClient;
  private String mToken;
  private User mCurrentUser;

  public AnnouncementManager(Context context) {
    mContext = context;
    mKD = new AnnouncementDatabase(context);
  }

  public CancellableCallable<LastModifiedList<Announcement>> getAnnouncementsCall() {
    Log.d(TAG, "getAnnouncementsCall");
    return mAnnouncementRestClient.search(mAnnouncementsLastUpdate);
  }

  public List<Announcement> executeAnnouncementsCall(CancellableCallable<LastModifiedList<Announcement>> getAnnouncementsCall) throws Exception {
    Log.d(TAG, "execute getAnnouncements...");
    LastModifiedList<Announcement> result = getAnnouncementsCall.call();
    List<Announcement> announcements = result.getList();
    if (announcements != null) {
      mAnnouncementsLastUpdate = result.getLastModified();
      updateCachedAnnouncements(announcements);
      notifyObservers();
    }
    return cachedAnnouncementsByUpdated();
  }

  public AnnouncementLoader getAnnouncementLoader() {
    Log.d(TAG, "getAnnouncementLoader...");
    return new AnnouncementLoader(mContext, this);
  }

  public void setAnnouncementRestClient(AnnouncementRestClient announcementRestClient) {
    mAnnouncementRestClient = announcementRestClient;
  }

  public Cancellable getAnnouncementsAsync(final OnSuccessListener<List<Announcement>> successListener, OnErrorListener errorListener) {
    Log.d(TAG, "getAnnouncementsAsync...");
    return mAnnouncementRestClient.searchAsync(mAnnouncementsLastUpdate, new OnSuccessListener<LastModifiedList<Announcement>>() {

      @Override
      public void onSuccess(LastModifiedList<Announcement> result) {
        Log.d(TAG, "getAnnouncementsAsync succeeded");
        List<Announcement> announcements = result.getList();
        if (announcements != null) {
          mAnnouncementsLastUpdate = result.getLastModified();
          updateCachedAnnouncements(announcements);
        }
        successListener.onSuccess(cachedAnnouncementsByUpdated());
        notifyObservers();
      }
    }, errorListener);
  }

  public Cancellable getAnnouncementAsync(
      final String announcementId,
      final OnSuccessListener<Announcement> successListener,
      final OnErrorListener errorListener) {
    Log.d(TAG, "getAnnouncementAsync...");
    return mAnnouncementRestClient.readAsync(announcementId, new OnSuccessListener<Announcement>() {

      @Override
      public void onSuccess(Announcement announcement) {
        Log.d(TAG, "getAnnouncementAsync succeeded");
        if (announcement == null) {
          setChanged();
          mAnnouncements.remove(announcementId);
        } else {
          if (!announcement.equals(mAnnouncements.get(announcement.getId()))) {
            setChanged();
            mAnnouncements.put(announcementId, announcement);
          }
        }
        successListener.onSuccess(announcement);
        notifyObservers();
      }
    }, errorListener);
  }

  public Cancellable saveAnnouncementAsync(
      final Announcement announcement,
      final OnSuccessListener<Announcement> successListener,
      final OnErrorListener errorListener) {
    Log.d(TAG, "saveAnnouncementAsync...");
    return mAnnouncementRestClient.updateAsync(announcement, new OnSuccessListener<Announcement>() {

      @Override
      public void onSuccess(Announcement announcement) {
        Log.d(TAG, "saveAnnouncementAsync succeeded");
        mAnnouncements.put(announcement.getId(), announcement);
        successListener.onSuccess(announcement);
        setChanged();
        notifyObservers();
      }
    }, errorListener);
  }

  public void subscribeChangeListener() {
    mAnnouncementSocketClient.subscribe(new ResourceChangeListener<Announcement>() {
      @Override
      public void onCreated(Announcement announcement) {
        Log.d(TAG, "changeListener, onCreated");
        ensureAnnouncementCached(announcement);
      }

      @Override
      public void onUpdated(Announcement announcement) {
        Log.d(TAG, "changeListener, onUpdated");
        ensureAnnouncementCached(announcement);
      }

      @Override
      public void onDeleted(String announcementId) {
        Log.d(TAG, "changeListener, onDeleted");
        if (mAnnouncements.remove(announcementId) != null) {
          setChanged();
          notifyObservers();
        }
      }

      private void ensureAnnouncementCached(Announcement announcement) {
        if (!announcement.equals(mAnnouncements.get(announcement.getId()))) {
          Log.d(TAG, "changeListener, cache updated");
          mAnnouncements.put(announcement.getId(), announcement);
          setChanged();
          notifyObservers();
        }
      }

      @Override
      public void onError(Throwable t) {
        Log.e(TAG, "changeListener, error", t);
      }
    });
  }

  public void unsubscribeChangeListener() {
    mAnnouncementSocketClient.unsubscribe();
  }

  public void setAnnouncementSocketClient(AnnouncementSocketClient AnnouncementSocketClient) {
    mAnnouncementSocketClient = AnnouncementSocketClient;
  }

  private void updateCachedAnnouncements(List<Announcement> announcements) {
    Log.d(TAG, "updateCachedAnnouncements");
    for (Announcement announcement : announcements) {
      mAnnouncements.put(announcement.getId(), announcement);
    }
    setChanged();
  }

  private List<Announcement> cachedAnnouncementsByUpdated() {
    ArrayList<Announcement> announcements = new ArrayList<>(mAnnouncements.values());
    Collections.sort(announcements, new AnnouncementByUpdatedComparator());
    return announcements;
  }

  public List<Announcement> getCachedAnnouncements() {
    return cachedAnnouncementsByUpdated();
  }

  public Cancellable loginAsync(
      String username, String password,
      final OnSuccessListener<String> successListener,
      final OnErrorListener errorListener) {
    final User user = new User(username, password);
    return mAnnouncementRestClient.getToken(
        user, new OnSuccessListener<String>() {

          @Override
          public void onSuccess(String token) {
            mToken = token;
            if (mToken != null) {
              user.setToken(mToken);
              setCurrentUser(user);
              mKD.saveUser(user);
              successListener.onSuccess(mToken);
            } else {
              errorListener.onError(new ResourceException(new IllegalArgumentException("Invalid credentials")));
            }
          }
        }, errorListener);
  }

  public void setCurrentUser(User currentUser) {
    mCurrentUser = currentUser;
    mAnnouncementRestClient.setUser(currentUser);
  }

  public User getCurrentUser() {
//    return mKD.getCurrentUser();
    return null;
  }

  private class AnnouncementByUpdatedComparator implements java.util.Comparator<Announcement> {
    @Override
    public int compare(Announcement n1, Announcement n2) {
      return (int) (n1.getUpdated() - n2.getUpdated());
    }
  }
}
