package com.example.alina.announcementapp.net.mapping;

public class Api {
  public static class Announcement {
    public static final String URL = "api/announcement";
    public static final String ANNOUNCEMENT_CREATED = "announcement/created";
    public static final String ANNOUNCEMENT_UPDATED = "announcement/updated";
    public static final String ANNOUNCEMENT_DELETED = "announcement/deleted";
    public static final String _ID = "_id";
    public static final String TEXT = "text";
    public static final String TITLE = "title";
    public static final String CATEGORY = "category";
    public static final String DATE = "date";
    public static final String UPDATED = "updated";
    public static final String USER_ID = "user";
    public static final String VERSION = "version";


  }
  public static class Auth {
    public static final String TOKEN = "token";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
  }
}
