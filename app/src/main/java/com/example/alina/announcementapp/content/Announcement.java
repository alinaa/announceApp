package com.example.alina.announcementapp.content;

public class Announcement {

  public enum Status {
    active,
    archived;

  }
  private String mId;
  private String mUserId;
  private String mText;
  private String mTitle;
  private String mCategory;
  private String mDate ;
  private long mUpdated;
  private int mNersion;

  public Announcement() {
  }

  public String getId() {
    return mId;
  }

  public void setId(String id) {
    mId = id;
  }

  public String getUserId() {
    return mUserId;
  }

  public void setUserId(String userId) {
    mUserId = userId;
  }

  public String getText() {
    return mText;
  }

  public void setText(String text) {
    mText = text;
  }

  public String getTitle() {
    return mTitle;
  }

  public void setTitle(String title) {
    mTitle = title;
  }
  public String getCategory() {
    return mCategory;
  }

  public void setCategory(String category) {
    mTitle = category;
  }
  public String getDate() {
    return mDate;
  }

  public void setDate(String date) {
    mTitle = date;
  }

  public long getUpdated() {
    return mUpdated;
  }

  public void setUpdated(long updated) {
    mUpdated = updated;
  }

  public int getVersion() {
    return mNersion;
  }

  public void setVersion(int version) {
    mNersion = version;
  }

  @Override
  public String toString() {
    return "Announcement{" +
        "mId='" + mId + '\'' +
        ", mUserId='" + mUserId + '\'' +
        ", mText='" + mText + '\'' +
        ", mTitle=" + mTitle +
        ", mCategory=" + mCategory +
        ", mDate=" + mDate +
        ", mUpdated=" + mUpdated +
        ", mNersion=" + mNersion +
        '}';
  }
}
